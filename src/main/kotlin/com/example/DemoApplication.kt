package com.example

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemoApplication {
	fun contextLoads() = 0

	companion object {
		@JvmStatic
		fun main(args: Array<String>) {
			runApplication<DemoApplication>(*args)
		}
	}
}
